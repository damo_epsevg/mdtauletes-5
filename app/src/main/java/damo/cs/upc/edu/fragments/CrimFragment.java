package damo.cs.upc.edu.fragments;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;

import java.util.UUID;

/**
 * Created by Josep M on 17/06/2016.
 */
public class CrimFragment extends FragmentObservable {


    private static final int EDICIO_CRIM = 120;

    private static String ARG = "id_Crim";

    private Crim crim;


    private EditText entradaTitol;
    private CheckBox checkSolucionat;
    private Button botoData;

    static CrimFragment getInstance(UUID idCrim) {
        CrimFragment f = new CrimFragment();
        Bundle b = new Bundle();
        b.putSerializable(ARG, idCrim);
        f.setArguments(b);
        return f;
    }

    private Crim crimAVisualitzar() {
        UUID idCrim = (UUID) getArguments().getSerializable(ARG);
        return MagatzemCrims.obtenirMagatzem().getCrim(idCrim);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.v("RETRO", "onCreate Fragment");
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.crim_fragment, container, false);

        crim = crimAVisualitzar();

        inicialitzarWidgets(v);

        pobla(v);

        programaWidgets(v);

        return v;

    }


    private void programaWidgets(View v) {

        entradaTitol.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                crim.setTitol(s.toString());
                avisaObservador(crim, EDICIO_CRIM);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


        checkSolucionat.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                crim.setSolucionat(isChecked);
                avisaObservador(crim, EDICIO_CRIM);
            }
        });
    }

    private void inicialitzarWidgets(View v) {
        entradaTitol = (EditText) v.findViewById(R.id.titol_crim);
        checkSolucionat = (CheckBox) v.findViewById(R.id.crime_solved);
        botoData = (Button) v.findViewById(R.id.crime_date);
    }


    private void pobla(View v) {
        if (v == null) return;
        entradaTitol.setText(crim.getTitol());
        botoData.setText(crim.getData().toString());
        checkSolucionat.setChecked(crim.getSolucionat());
    }


}
