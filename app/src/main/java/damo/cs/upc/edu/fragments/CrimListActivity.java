package damo.cs.upc.edu.fragments;


import android.app.Fragment;
import android.app.FragmentManager;
import android.os.Bundle;
import android.util.Log;

import java.util.UUID;

/**
 * Created by josepm on 30/6/16.
 */
public class CrimListActivity extends SingleFragmentActivity {

    private static final int NOVA_SELECCIÖ = 100;
    private static final int MODIF_CRIM = 110;
    private static final int EDICIO_CRIM = 120;

    private static final String DARRER_ID_CRIM = "Darrer id crim";

    private UUID idDarrerVisualitzat = null;

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        // Protegim pel cas que hi hagi una rotació sense haver seleccionat prèviament cap crim
        if (idDarrerVisualitzat != null) {
            outState.putString(DARRER_ID_CRIM, idDarrerVisualitzat.toString());
        }
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        Log.v("RETRO", "onRestoreInstance activity");
        String s = savedInstanceState.getString(DARRER_ID_CRIM);
        if (s != null) {
            idDarrerVisualitzat = UUID.fromString(s);
        }


        // Enregistrem l'activity com a  observadora del fragment que mostra el detall
        Fragment fragment = getFragmentManager().findFragmentById(R.id.contenidor_detall);
        if (fragment != null) {
            ((FragmentObservable) fragment).setObservadorFragment(new FragmentObservable.ObservadorFragment() {
                @Override
                public void onCanviFragment(Crim crim, int idCrida) {
                    modificaLlistat();
                }
            });
        }
    }


    @Override
    protected int getLayoutResId() {
        return R.layout.mestredetall;
    }

    @Override
    protected int getContenidorFragmentResId() {
        return R.id.contenidor;
    }

    @Override
    protected Fragment getInstance() {
        return new CrimListFragment();
    }

    @Override
    public void onCanviFragment(Crim crim, int idCrida) {
        switch (idCrida) {
            case MODIF_CRIM:
                if (idDarrerVisualitzat == null) return;
                if (idDarrerVisualitzat.compareTo(crim.getId()) == 0)
                    mostraDetall(crim);

                break;
            case NOVA_SELECCIÖ:
                idDarrerVisualitzat = crim.getId();
                mostraDetall(crim);
                break;
        }
    }

    private void modificaLlistat() {
        CrimListFragment fragment = (CrimListFragment) getFragmentManager().findFragmentById(R.id.contenidor);
        fragment.modificaLlistat();
    }


    private void mostraDetall(Crim crim) {

        Fragment fragment = CrimFragment.getInstance(crim.getId());

        FragmentManager fragmentManager = getFragmentManager();

        fragmentManager.beginTransaction().replace(R.id.contenidor_detall, fragment).commit();

        ((CrimFragment) fragment).setObservadorFragment(new FragmentObservable.ObservadorFragment() {
            @Override
            public void onCanviFragment(Crim crim, int idCrida) {
                modificaLlistat();
            }
        });


    }


}
