package damo.cs.upc.edu.fragments;

import android.app.Fragment;
import android.util.Log;

/**
 * Created by josepm on 27/9/16.
 */
public class FragmentObservable extends Fragment {
    private ObservadorFragment observador;

    public void setObservadorFragment(ObservadorFragment observadorFragment) {
        observador = observadorFragment;
    }

    protected void avisaObservador(Crim crim, int idCrida) {
        if (observador == null) return;
        Log.v("INFO", "onCanviFragment");
        observador.onCanviFragment(crim, idCrida);
    }

    public interface ObservadorFragment {
        void onCanviFragment(Crim crim, int idCrida);
    }
}
