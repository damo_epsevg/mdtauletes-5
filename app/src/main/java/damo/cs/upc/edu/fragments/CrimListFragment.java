package damo.cs.upc.edu.fragments;


import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

/**
 * Created by josepm on 30/6/16.
 */
public class CrimListFragment extends FragmentObservable {


    private static final String UUI_DETALL = "id_Crim";
    private static final String DARRERA_POSICIO = "Darrera posicio"; // Estat del fragment

    private static final int NOVA_SELECCIÖ = 100;
    private static final int MODIF_CRIM = 110;


    private CrimAdapter adapter;

    private RecyclerView recyclerView;
    private int posicio;  // Darrera posició seleccionada


    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        Log.v("LOG", "onSavedinstance CrimListFragment");
        outState.putInt(DARRERA_POSICIO, posicio);


    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.v("LOG", "onCreate CrimListFragment");

        if (savedInstanceState != null) {
            posicio = savedInstanceState.getInt(DARRERA_POSICIO);
        }

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.list_crim_fragment, container, false);

        // On es visualitza la llista de crims
        recyclerView = (RecyclerView) v.findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        actualitzaUI();

        return v;
    }

    private void actualitzaUI() {

        // Acabem d'arrencar el fragment. Podria anar a onCreateView, ja que abans no ho necesitem
        if (adapter == null) {
            adapter = new CrimAdapter(MagatzemCrims.obtenirMagatzem());
            recyclerView.setAdapter(adapter);
        } else
            adapter.notifyItemChanged(posicio);
    }

    private void programarWidgets(final CrimHolder holder, final int position) {
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mostraDetall(holder.crim, position);
            }
        });

        holder.solucionat_fila.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                actualitzaEstatCrim(holder.crim, isChecked, position);
            }
        });
    }

    public void modificaLlistat() {
        recyclerView.getAdapter().notifyItemChanged(posicio);
    }

    // Actualitza el model i la presentació
    private void actualitzaEstatCrim(Crim crim, boolean isChecked, int posicio) {
        crim.setSolucionat(isChecked);
        avisaObservador(crim, MODIF_CRIM);
    }

    private void mostraDetall(final Crim crim, final int posicio) {
        this.posicio = posicio;
        avisaObservador(crim, NOVA_SELECCIÖ);
    }

    class CrimAdapter extends RecyclerView.Adapter<CrimHolder> {

        MagatzemCrims crims;

        public CrimAdapter(MagatzemCrims crims) {
            this.crims = crims;

        }


        @Override
        public CrimHolder onCreateViewHolder(ViewGroup parent, int viewType) {


            LayoutInflater layoutInflater = LayoutInflater.from(getActivity());

            View view = layoutInflater.inflate(R.layout.fila_crim, parent, false);

            return new CrimHolder(view);
        }


        @Override
        public void onBindViewHolder(CrimHolder holder, int position) {
            Crim crim = crims.getCrim(position);

            holder.bindCrim(crim);

            programarWidgets(holder, position);
        }


        @Override
        public int getItemCount() {
            return crims.getCount();
        }


    }

    class CrimHolder extends RecyclerView.ViewHolder {
        private Crim crim;


        private TextView titol_fila;
        private TextView data_fila;
        private CheckBox solucionat_fila;


        public CrimHolder(View view) {
            super(view);
            titol_fila = (TextView) view.findViewById(R.id.titol_crim_fila);
            data_fila = (TextView) view.findViewById(R.id.data_crim_fila);
            solucionat_fila = (CheckBox) view.findViewById(R.id.solucionat_fila);


        }


        public void bindCrim(Crim crim) {
            this.crim = crim;
            titol_fila.setText(crim.getTitol());
            data_fila.setText(crim.getData().toString());
            solucionat_fila.setChecked(crim.getSolucionat());
        }

    }


}
